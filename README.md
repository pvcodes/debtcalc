
<h1 align=center>Debt Calc</h1>
An android app for debt calculation, based on <i>koltin</i>. 

It's license fall under the [MIT LICENSE](LICENSE.md).





<h2>Screenshots</h2>
<img src=https://gitlab.com/pvcodes/debtcalc/uploads/2b9907716c777e9e40a4ba064c378138/dc3.jpg width=500>

<img src=https://gitlab.com/pvcodes/debtcalc/uploads/764034c24f50ad8b6e815727307b04e9/dc2.jpg width=500>

<img src=https://gitlab.com/pvcodes/debtcalc/uploads/757c81943b12cd5cb54a1754a923ffc0/dc1.jpg width=500>


<samp>
  <p align="center">
    ════ ⋆★⋆ ════<br>
    From <a href="https://github.com/pvcodes/pvcodes">pvcodes</a>
  </p>
</samp>
