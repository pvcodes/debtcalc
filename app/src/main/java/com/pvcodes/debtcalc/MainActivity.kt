package com.pvcodes.debtcalc

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import com.pvcodes.debtcalc.databinding.ActivityMainBinding
import java.math.RoundingMode
import java.time.LocalDate
import java.time.Period
import java.util.*
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val dateBtn: Button = binding.dateBtn
        val closeBtn: Button = binding.closeBtn
        val frontView: ConstraintLayout = binding.frontView
        val calenderView: LinearLayout = binding.calanderView
        val calcBtn: Button = binding.calcBtn
        val date: DatePicker = binding.datePicker
        val oneFourthHissab: Switch = binding.forMonth
        val displayArea: ConstraintLayout = binding.displayArea

//        On Startup View Settings
//        binding.footer.setMovementMethod(LinkMovementMethod.getInstance());
        setupLinkButton()

//        binding.footer

        displayArea.visibility = View.GONE
        date.maxDate = Calendar.getInstance().timeInMillis
        oneFourthHissab.isChecked = true

        var d1 = 0
        var m1 = 0
        var y1 = 0

        var principleAmt = 0
        var debtRate = 0.0


//        BTN Listeners
        dateBtn.setOnClickListener {
            frontView.visibility = View.INVISIBLE
            calenderView.visibility = View.VISIBLE
            it.hideKeyboard()
        }

        closeBtn.setOnClickListener {
            frontView.visibility = View.VISIBLE
            calenderView.visibility = View.INVISIBLE
            d1 = date.dayOfMonth
            m1 = date.month + 1
            y1 = date.year
            //
            dateBtn.text = "${d1}/${m1}/${y1}"

        }


        calcBtn.setOnClickListener {

            it.hideKeyboard()

            var tmp = binding.principleAmount.text.toString()
            if (tmp.isNotEmpty()) {
                principleAmt = tmp.toInt()
            }

            tmp = binding.debtRate.text.toString()
            if (tmp.isNotEmpty()) {
                debtRate = tmp.toDouble()
            }

            if (d1 == 0) {
                Toast.makeText(this, "Please, select the date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val debtPair =
                this.calculateDebt(d1, m1, y1, principleAmt, debtRate, oneFourthHissab.isChecked)
            binding.debtDisplay.text =
                "Total Debt : ₹ ${debtPair.first.roundToInt()} (${debtPair.second} months)"
            binding.totalAmtDIsplay.text =
                "Total Amount to be paid : ₹  ${(debtPair.first + principleAmt).roundToInt()}"
            displayArea.visibility = View.VISIBLE
        }

    }

    private fun View.hideKeyboard() {
        val inputManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun calculateDebt(
        d1: Int,
        m1: Int,
        y1: Int,
        principle: Int,
        rate: Double,
        oneFourthHissab: Boolean
    ): Pair<Double, Double> {

        if (principle == 0 || rate == 0.0) {
            return Pair(0.0, 0.0)
        }

        val today: Calendar = Calendar.getInstance()
        val d2 = today.get(Calendar.DAY_OF_MONTH)
        val m2 = today.get(Calendar.MONTH) + 1
        val y2 = today.get(Calendar.YEAR)

        val D2 = LocalDate.of(y2, m2, d2)
        val D1 = LocalDate.of(y1, m1, d1)

        val diff = Period.between(D1, D2)

        var totalMonths = (diff.years * 12 + diff.months).toDouble()

        var daysDIff = (diff.days)

        totalMonths += if (oneFourthHissab) {

            when (daysDIff) {
                in 0..8 ->
                    .25
                in 9..15 ->
                    .5
                in 16..25 -> .75
                else -> 1.0
            }
        } else {
            daysDIff.toDouble() / 30
        }

        totalMonths = totalMonths.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
//        Toast.makeText(this, "$totalMonths  ", Toast.LENGTH_SHORT).show()

        if (totalMonths < 1.0) totalMonths = 1.0

        val totalDebt = ((principle*totalMonths*rate)/100).toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

        return Pair(totalDebt, totalMonths)
    }


    private fun setupLinkButton() {
        val linkButton = binding.footerDisplay
        linkButton.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://pvcodes.me"))
            startActivity(browserIntent)
        }
    }

}


